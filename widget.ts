﻿interface IProjectActivity {
  type: string;
  name: string;
}
import {
  AfterViewInit,
  Component,
  Input,
  NgModule,
  OnInit
} from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import {
  IWidgetComponent,
  IWidgetContext2,
  IWidgetInstance2,
  StringUtil,
  Log
} from "lime";

import lm = require("lime");
import { SohoListViewComponent } from "scripts/typings/soho";
declare var $: jQuery;

// Search Pipe
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filterpjat"
})
export class FilterPjPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;
    searchText = searchText.toLowerCase();
    return items.filter(it => {
      return it.description.toLowerCase().includes(searchText);
    });
  }
}

@Component({
  template: `
  <div id="wbs-listview-search" class="listview-search listview-search-fixed mysearch">
      <label id="projectactivityid" for="projectactivityid-beforeinput">Project Activity ID:</label>
      <input [(ngModel)]="pjactId" name="searchfield" id="projectactivityid" style="padding-left: 155px"  class="searchfield" maxlength="9" aria-required="true" data-validate="required" xi-validate="" required="" autocomplete="off">
      <button type="submit" class="btn-icon get-icon" title="Get" [disabled]="!pjactId" (click)="sendRequestLN(pjactId)">
        <svg class="icon" focusable="false" aria-hidden="true" role="presentation">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search"></use>
        </svg>
      </button>
      <button type="button" class="btn-icon drilldown-icon" title="Drilldown" [disabled]="!pjactId" (click)="drillback()">
        <svg class="icon" focusable="false" aria-hidden="true" role="presentation">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-drilldown"></use>
        </svg>
      </button>
  </div>


<div [hidden]="alert"  class="listview listview-top-hard" id="wbs-listview">
          <ul>
          <li class="listview disable-hover" *ngFor="let entry of listviewSearchOptions | filterpjat : searchText"
              [ngClass]="
            {'status-labor-leftborder': entry.costtype=='Labor',
            'status-materials-leftborder': entry.costtype=='Materials',
            'status-equipment-leftborder': entry.costtype=='Equipment',
            'status-subcontracting-leftborder': entry.costtype=='Subcontracting',
            'status-sundrycosts-leftborder': entry.costtype=='Sundry Costs'
            }"
          >
								<div class="listview-wrapper row">
									<div class="three columns">
                    <p class="listview-heading tag" 
                    [ngClass]="{'status-labor': entry.costtype=='Labor',
                    'status-materials': entry.costtype=='Materials',
                    'status-equipment': entry.costtype=='Equipment',
                    'status-subcontracting': entry.costtype=='Subcontracting',
                    'status-sundrycosts': entry.costtype=='Sundry Costs'
                    }">
                    {{entry.costtype}}
                    
                    </p>
									</div>
									<div class="nine columns">
										<div class="columns" style="float:right; width: auto;margin:0 0 0 40px;text-align:right;">
											<p class="listview-heading">Excl Commit Actual</p>
                      <p class="listview-micro">
                      {{entry.exclCommitActual}}
                      </p>
										</div>

										<div class="columns" style="float:right; width: auto; margin-left: 40px;text-align:right;">
											<p class="listview-heading">Earned Value</p>
                      <p class="listview-micro">
                      {{entry.earnedValue}}
                      </p>
										</div>

										<div class="columns" style="float:right; width: auto;text-align:right;">
											<p class="listview-heading">Total Budget</p>
                      <p class="listview-micro">
                      {{entry.totalbudget}}
                      </p>
										</div>
									</div>
								</div>
						</li>
					</ul>
				</div>


 
   <div [hidden]="!alert" class="busy lm-position-r">
        <div style="text-align: center; margin-top: 50px;">
        <svg *ngIf="alertmessage==='No Data has been received yet'" class="icon icon-empty-state" focusable="false" aria-hidden="true" role="presentation" style="margin-right:7px;">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-no-data" href="#icon-empty-no-data"></use>
        </svg>
        <svg *ngIf="alertmessage==='An unexpected error occurred'"  class="icon icon-empty-state" focusable="false" aria-hidden="true" role="presentation" style="margin-right:7px;">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-error-loading" href="#icon-empty-error-loading"></use>
        </svg>
        <svg *ngIf="alertmessage==='No data found on WBS Costing'"  class="icon icon-empty-state" focusable="false" aria-hidden="true" role="presentation" style="margin-right:7px;">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-no-notes" href="#icon-empty-no-notes"></use>
      </svg>
      </div>

      <div style="text-align: center;margin-top: 31px;">
          <h4 style="color:#1a1a1a;margin-bottom:6px;">{{alertmessage}}</h4>
          <div style="color:#5c5c5c; font-size: 14px;">{{alertmessagesub}}</div>
      </div>
   </div>


   <svg xmlns="http://www.w3.org/2000/svg" class="svg-icons" style="display: none">
   <symbol id="icon-empty-no-data" viewBox="0 0 65 65">
     <path d="M48 63h-7c-2.209 0-4-1.791-4-4v-41c0-2.209 1.791-4 4-4h7c2.209 0 4 1.791 4 4v41c0 2.209-1.791 4-4 4m-17 0h-7c-2.209 0-4-1.791-4-4v-41c0-2.209 1.791-4 4-4h7c2.209 0 4 1.791 4 4v41c0 2.209-1.791 4-4 4m-17 0h-7c-2.209 0-4-1.791-4-4v-41c0-2.209 1.791-4 4-4h7c2.209 0 4 1.791 4 4v41c0 2.209-1.791 4-4 4" class="icon-empty-offset-color is-personalizable"></path>
     <path d="M62.726 16.877l-7.002-12.827c-.771-1.416-2.677-1.392-3.448.024l-7.002 12.791c-.775 1.427.184 3.135 1.724 3.135h14.004c1.54 0 2.499-1.696 1.724-3.123" class="icon-empty-main-color is-personalizable"></path>
     <path d="M55 12.5c0 .276-.178.5-.471.5h-1.058c-.293 0-.471-.224-.471-.5v-4c0-.276.178-.0.671-.5h1.058c.293 0 .471.224.471.5v4zm0 4c0 .276-.178.5-.471.5h-1.058c-.293 0-.471-.224-.471-.5v-1c0-.276.178-.0.671-.5h1.058c.293 0 .471.224.471.5v1z" class="icon-empty-center-color is-personalizable"></path>
   </symbol>
 </svg>
 <svg xmlns="http://www.w3.org/2000/svg" class="svg-icons" style="display: none">
   <symbol id="icon-empty-error-loading" viewBox="0 0 65 65">
     <path d="M33.25 14.564l-2.703-.552c-.258-.051-.461-.243-.538-.494-.226-.706-.51-1.39-.854-2.046-.124-.233-.117-.511.029-.733l1.525-2.302c.573-.867.457-2.018-.278-2.753l-1.113-1.114c-.737-.735-1.888-.852-2.753-.279l-2.304 1.525c-.222.144-.5.152-.735.029-.654-.346-1.336-.628-2.044-.853-.251-.08-.441-.281-.494-.539l-.552-2.703c-.208-1.019-1.104-1.75-2.143-1.75h-1.586c-1.039 0-1.935.731-2.143 1.75l-.552 2.703c-.053.258-.243.459-.494.539-.708.225-1.39.507-2.044.853-.235.123-.513.115-.735-.029l-2.304-1.525c-.865-.573-2.016-.456-2.753.279l-1.113 1.114c-.735.735-.851 1.886-.278 2.753l1.525 2.302c.146.222.153.5.029.733-.344.656-.628 1.34-.854 2.046-.077.251-.28.443-.538.494l-2.703.552c-1.019.208-1.75 1.104-1.75 2.145v1.582c0 1.041.731 1.937 1.75 2.145l2.703.552c.258.051.461.243.538.494.226.706.51 1.391.854 2.044.124.235.117.513-.029.735l-1.525 2.302c-.573.867-.457 2.019.278 2.754l1.113 1.114c.735.735 1.888.851 2.753.278l2.304-1.525c.222-.146.5-.151.735-.029.654.346 1.336.628 2.044.854.251.078.441.28.494.536l.552 2.705c.208 1.019 1.104 1.75 2.143 1.75h1.586c1.039 0 1.935-.731 2.143-1.75l.552-2.705c.053-.256.243-.458.494-.536.708-.226 1.39-.508 2.044-.854.235-.122.513-.117.735.029l2.304 1.525c.865.573 2.018.457 2.753-.278l1.113-1.114c.735-.735.851-1.887.278-2.754l-1.525-2.302c-.146-.222-.153-.5-.029-.735.344-.653.628-1.338.854-2.044.077-.251.28-.443.538-.494l2.703-.552c1.019-.208 1.75-1.104 1.75-2.145v-1.582c0-1.041-.731-1.937-1.75-2.145m-15.75 8.769c-3.222 0-5.833-2.611-5.833-5.833s2.611-5.833 5.833-5.833 5.833 2.611 5.833 5.833-2.611 5.833-5.833 5.833" class="icon-empty-main-color is-personalizable"></path>
     <path d="M42.234 24.807c-.604.728-.502 1.822.225 2.426.726.602 1.821.502 2.424-.226.604-.726.501-1.821-.225-2.424-.727-.603-1.82-.503-2.424.224m10.149-9.53c-.808-.671-1.625-.641-2.309.184l-1.555 1.874c-.201.242-.383.528-.567.812l-2.636 4.196c-.326.523-.205.841.214 1.189.42.349.753.407 1.207-.008l3.639-3.364c.246-.232.494-.463.695-.705l1.556-1.874c.684-.825.564-1.634-.244-2.304" class="icon-empty-main-color is-personalizable"></path>
     <g transform="translate(3 5)">
         <path d="M58.45 37.899l-2.394-.489c-.229-.045-.408-.215-.477-.438-.2-.625-.451-1.231-.756-1.811-.11-.207-.103-.453.026-.65l1.35-2.039c.508-.768.405-1.787-.246-2.438l-.985-.987c-.653-.651-1.673-.755-2.439-.246l-2.041 1.35c-.196.127-.442.134-.651.026-.578-.307-1.183-.557-1.81-.757-.222-.07-.391-.248-.437-.477l-.489-2.393c-.185-.903-.979-1.55-1.898-1.55h-1.406c-.919 0-1.713.647-1.898 1.55l-.489 2.393c-.046.229-.210.607-.437.477-.627.2-1.231.45-1.81.757-.209.108-.455.101-.651-.026l-2.041-1.35c-.766-.509-1.786-.405-2.439.246l-.985.987c-.651.651-.754 1.67-.246 2.438l1.35 2.039c.129.197.136.443.026.65-.305.58-.556 1.186-.756 1.811-.069.223-.248.393-.477.438l-2.394.489c-.902.184-1.55.978-1.55 1.9v1.401c0 .922.648 1.716 1.55 1.9l2.394.489c.229.040.608.216.477.438.2.620.651 1.231.756 1.81.11.208.103.454-.026.651l-1.35 2.039c-.508.768-.405 1.787.246 2.438l.985.987c.651.651 1.673.755 2.439.247l2.041-1.351c.196-.129.442-.134.651-.025.579.306 1.183.556 1.81.756.222.069.391.248.437.475l.489 2.396c.185.902.979 1.55 1.898 1.55h1.406c.919 0 1.713-.648 1.898-1.55l.489-2.396c.046-.227.215-.406.437-.475.627-.2 1.232-.45 1.81-.756.209-.109.455-.104.651.025l2.041 1.351c.766.508 1.788.404 2.439-.247l.985-.987c.651-.651.754-1.67.246-2.438l-1.35-2.039c-.129-.197-.136-.443-.026-.651.305-.579.556-1.185.756-1.81.069-.222.248-.393.477-.438l2.394-.489c.902-.184 1.55-.978 1.55-1.9v-1.401c0-.922-.648-1.716-1.55-1.9m-13.95 7.767c-2.854 0-5.167-2.313-5.167-5.166 0-2.854 2.313-5.167 5.167-5.167 2.854 0 5.167 2.313 5.167 5.167 0 2.853-2.313 5.166-5.167 5.166" class="icon-empty-offset-color is-personalizable" mask="url(#d)"></path>
     </g>
   </symbol>
 </svg>

 <svg xmlns="http://www.w3.org/2000/svg" class="svg-icons" style="display: none">
   <symbol id="icon-empty-no-notes" viewBox="0 0 65 65">
   <path d="M3 13h60v-10h-60v10z" class="icon-empty-center-color is-personalizable" style="fill:#368ac0;"></path>
   <path d="M53 53v10l10-10h-10zm-50-38v48h48v-12h12v-36h-60z" class="icon-empty-offset-color is-personalizable"></path>
   <path d="M39.414 41.586c.781.781.781 2.047 0 2.828-.391.391-.902.586-1.414.586-.512 0-1.024-.195-1.414-.586l-4.086-4.086-4.086 4.086c-.391.391-.902.586-1.414.586-.512 0-1.024-.195-1.414-.586-.781-.781-.781-2.047 0-2.828l4.086-4.086-4.086-4.086c-.781-.781-.781-2.047 0-2.828.781-.781 2.047-.781 2.828 0l4.086 4.086 4.086-4.086c.781-.781 2.047-.781 2.828 0 .781.781.781 2.047 0 2.828l-4.086 4.086 4.086 4.086z" class="icon-empty-center-color is-personalizable"></path>
   </symbol>
   </svg>
  `,
  styles: [
    `
    #wbs-listview  .tag{
      padding-top:2px;
    }

    .listview li {
      cursor:pointer;
    }
    .status-labor{
      background:#A6D786;
      color:white;
    }
    .status-labor-leftborder{
      border-left:solid 4px #A6D786;
    }
    .status-materials{
      background:#83CBBF;
      color:white;
    }
    .status-materials-leftborder{
      border-left:solid 4px #83CBBF;
    }
    .status-subcontracting{
      background:#C0ABD6;
      color:white;
    }
    .status-subcontracting-leftborder{
      border-left:solid 4px #C0ABD6;
    }
    .status-equipment{
      background:#82C2E2;
      color:white;
    }
    .status-equipment-leftborder{
      border-left:solid 4px #82C2E2;
    }
    .status-sundrycosts{
      background:#F5D070;
      color:white;
    }
    .status-sundrycosts-leftborder{
      border-left:solid 4px #F5D070;
    }
    #wbs-listview-search{
      position:relative;
    }

    #wbs-listview-search > button[type="submit"]{
    position: absolute;
    line-height: 16px;
    top: 0;
    right: 36px;
    }
    #wbs-listview-search > button[type="button"]{
      position: absolute;
      line-height: 16px;
      top: 0;
      right: 8px;
      }
      #wbs-listview-search > label{
        position: absolute;
        color: #999;
        line-height: 16px;
        cursor: text;
        font-size:14px;
        padding-left:30px;
        top:10px;
      }
  `
  ]
})
export class WBSCostingComponent implements AfterViewInit, IWidgetComponent {
  @Input() widgetContext: IWidgetContext2;
  @Input() widgetInstance: IWidgetInstance2;
  private listviewSearchOptions = [];
  private logPrefix: string;
  private messagetype: string;
  private pageId: string;
  private username: string;
  private password: string;
  private company: string;
  private webservice: string;
  private logicalId: string;
  private lang: lm.ILanguage;
  private isHandlerRegistered: boolean;
  private sessionId: string;

  private pjactId: string;

  private alert: boolean = true;
  private alertmessage: string = "No Data has been received yet";
  private alertmessagesub: string = "User another widget to get data";

  private searchText: string = "";

  constructor() {}
  ngAfterViewInit() {
    this.logPrefix = "[" + this.widgetContext.getId() + "] ";
    this.lang = this.widgetContext.getLanguage();
    const pageId = this.widgetContext.getPageId();
    this.pageId = pageId;
    // Subscribe to the event that is triggered when settings are saved to be able to update the message text
    this.widgetInstance.settingsSaved = () => {
      this.updateMessageType();
    };

    // Initial update of the message text and color
    this.updateMessageType();
  }

  private unregisterHandler(messageType: string): void {
    infor.companyon.client.unRegisterMessageHandler(messageType);

    Log.debug(
      this.logPrefix +
        "Message handler unregistered for message type: " +
        messageType
    );
  }

  private updateMessageType(): void {
    let messagetype = this.widgetContext
      .getSettings()
      .get<string>("messagetype");
    const newMessageType = messagetype + this.pageId;
    this.username = this.widgetContext.getSettings().get<string>("user");
    this.password = this.widgetContext.getSettings().get<string>("password");
    this.webservice = this.widgetContext
      .getSettings()
      .get<string>("webservice");
    this.company = this.widgetContext.getSettings().get<string>("company");
    this.logicalId = this.widgetContext.getSettings().get<string>("logicalid");
    this.sessionId = this.widgetContext.getSettings().get<string>("sessionid");
    // register handler
    const original = messagetype;
    if (
      !StringUtil.isNullOrWhitespace(messagetype) &&
      newMessageType !== original
    ) {
      if (this.isHandlerRegistered) {
        this.unregisterHandler(original);
      }
      this.registerHandler(newMessageType);
    }
  }

  private registerHandler(messageType: string): void {
    const callback = (args: IProjectActivity) => {
      if (args.type === "pjname") {
        let name = args.name;
        this.pjactId = name;
        this.sendRequestLN(name);
      }
      Log.debug(
        this.logPrefix +
          "Message handler registered for message type: " +
          messageType +
          " " +
          JSON.stringify(args)
      );
    };
    infor.companyon.client.registerMessageHandler(messageType, callback);
    this.messagetype = messageType;
    this.isHandlerRegistered = true;
  }

  private getProjects(response) {
    this.setBusy(false);
    let wbsList = [];
    $(response.data)
      .find("Output")
      .each(function(key, value) {
        let costtype = $(value)
          .find("NameValue")
          .filter("[name='cotp']")
          .attr("desc");
        let totalbudget = $(value)
          .find("NameValue")
          .filter("[name='ambg']")
          .text();
        let earnedValue = $(value)
          .find("NameValue")
          .filter("[name='ampm']")
          .text();
        let exclCommitActual = $(value)
          .find("NameValue")
          .filter("[name='amac']")
          .text();

        wbsList.push({
          costtype: costtype,
          totalbudget: totalbudget,
          earnedValue: earnedValue,
          exclCommitActual: exclCommitActual
        });
      });
    wbsList.length > 0 ? (this.alert = false) : (this.alert = true),
      (this.alertmessage = this.lang.get("nodata")),
      (this.alertmessagesub = this.lang.get("nodatasubmessage"));
    this.listviewSearchOptions = wbsList;
  }

  private setBusy(isBusy: boolean): void {
    this.widgetContext.setState(
      isBusy ? lm.WidgetState.busy : lm.WidgetState.running
    );
  }

  private drillback() {
    if (
      this.pjactId != null &&
      this.pjactId != "" &&
      this.pjactId != "undefined"
    ) {
      var drillbackUrl =
        "?LogicalId=" +
        this.logicalId +
        "&ICMDrillback=true&Session=" +
        this.sessionId +
        "&SessionIndex=1&Filter=" +
        btoa(this.pjactId) +
        "&Enc=1&Mode=64";
      infor.companyon.client.sendPrepareDrillbackMessage(drillbackUrl);
      Log.debug("Drillback message sent to " + drillbackUrl);
    }
  }

  public sendRequestLN(projectname) {
    this.setBusy(true);
    const request = this.createRequest(projectname);
    this.widgetContext.executeIonApiAsync(request).subscribe(
      response => {
        this.getProjects(response);
      },
      error => {
        this.alert = true;
        this.alertmessage = this.lang.get("error");
        this.alertmessagesub = this.lang.get("errorsubmessage");
        this.setBusy(false);
      }
    );
  }
  private createRequest(projectname): lm.IIonApiRequestOptions {
    const request: lm.IIonApiRequestOptions = {
      method: "POST",
      data:
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:pur="http://www.infor.com/businessinterface/GenericQuery" >' +
        "<soapenv:Header>" +
        "<pur:Activation>" +
        "<username>" +
        this.username +
        "</username>" +
        "<password>" +
        this.password +
        "</password>" +
        "<company>" +
        this.company +
        "</company>" +
        "</pur:Activation>" +
        "</soapenv:Header>" +
        "<soapenv:Body>" +
        "<pur:Show>" +
        "<ShowRequest>" +
        "<DataArea>" +
        "<GenericQuery>" +
        "<Definition>" +
        "select tpppc401.cprj:cprj,tpppc401.cotp:cotp ,tpppc401.ambg:ambg,tpppc401.ampm:ampm, tpppc401.amac:amac from tpppc401 where tpppc401.cprj='" +
        projectname +
        "'" +
        "</Definition>" +
        "</GenericQuery>" +
        "</DataArea>" +
        "</ShowRequest>" +
        "</pur:Show>" +
        "</soapenv:Body>" +
        "</soapenv:Envelope>",
      url: this.webservice,
      cache: false,
      headers: {
        Accept: "text/html",
        "Content-Type": 'text/xml; charset="utf-8"'
      },
      responseType: 'text/xml; charset="utf-8"',
      params: "",
      timeout: 30000
    };
    return request;
  }
}

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [WBSCostingComponent, FilterPjPipe],
  entryComponents: [WBSCostingComponent]
})
export class WBSCostingModule {}

// Widget factory function
export var widgetFactory = (context: IWidgetContext2): IWidgetInstance2 => {
  return {
    angularConfig: {
      moduleType: WBSCostingModule,
      componentType: WBSCostingComponent
    }
  };
};
